// COMMENTS in JavaScript
// - To write comments, we use two forward slash for single line comments and two forward slash and two asterisks for multi-line comments.
// This is a sing-line comment.
/*
This is a multi-line comment.
We add two asterisks inside the forward slashes.
*/

// VARIABLE
/*
	- Variables contain values that can be changed over the execution of the program.
	- To declare a variable, we use the "let" keyword.
*/

// let productName = 'desktop computer';
// console.log(productName);
// productName = 'cellphone'
// console.log(productName);


// CONSTANTS
/*
	-Use constants for values that will not change.
*/
// const deliveryFee = 30;
// console.log(deliveryFee);


// DATA TYPES

// 1. STRINGS
/*
	- Series of charactes that create a word, a phrase, sentence, or anything related to "TEXT".
	- Can be written using a single quote ('') or double quote ("")
	- On other programming languages, only the double quote can be used for creating strings.

*/

let country = 'Philippines';
let	province = "Metro Manila";

console.log(country + ", " + province);

// 2. NUMBERS

/*
	- Include integers/whole numbers, decimal numbers, fraction, exponential notation
*/
let	headCount = 26;
console.log(headCount);

let grade = 98.7;
console.log(grade);

let	planetDistance = 2e10;
console.log(planetDistance);

let PI = 22/7;
console.log(PI)

console.log(Math.PI);

console.log("John's grade last quarter is:" + grade)

// 3. BOOLEAN
/*
	-Boolean values are logical values.
	- Boolean values can contatain either "true" or "false"
*/

let isMarried = false;
let isGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log ("isGoodConduct: " + isGoodConduct);
console.log(isMarried);
console.log(isGoodConduct);


// 4. OBJECTS
	
	// Arrays

/*
	- A special kind of data that stores multiple values.
	- Are a special type of an object.
	- Can store different data types but us normally used to store similar data types.
*/

// Syntax:
	// let/const arrayName = [ElementA, ElementB, ElementC...]

let grades = [ 98.7, 92.1, 90.2, 94.6];
console.log(grades)
console.log(grades[0])

let userDetails = ["John", "Smith", 32, true]
	console.log(userDetails);

// Object Literals
/*

	- Another special kind of data type that mimics real world objects/items.
	- Used to create complex data that contains pieces of information that are relevant to each other.
	- Every individual piece of info is called a property of the object

	Syntax:

		let/const objectName = {propertyA: value,
		propertyB: value}
*/

	let personDetails = {
		fullName: 'Juan Dela Cruz',
		age: 35, 
		isMarried: false,
		contact: ['+639876543210', '024785425'],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}
	console.log(personDetails)